from trytond.pool import Pool
from . import gyneco
from . import evaluation


def register():
    Pool.register(
        gyneco.Patient,
        gyneco.PatientMenstrualHistory,
        gyneco.PatientMammographyHistory,
        gyneco.PatientPAPHistory,
        gyneco.PatientColposcopyHistory,
        gyneco.Perinatal,
        gyneco.PatientPregnancy,
        gyneco.PrenatalEvaluation,
        gyneco.PuerperiumMonitor,
        gyneco.PerinatalMonitor,
        evaluation.PatientEvaluation,
        evaluation.HealthEvaluationPatientPregnancy,
        module='health_gyneco', type_='model')
